#include<rost/markov.hpp>
#include <iostream>
#include <chrono>
using namespace std;
int main(){
	
	fast_random<> r;

	uint32_t n_bins = 10;
	vector<int> cdf(n_bins, 0.0);
	vector<float> pdf(cdf.size(),0.0);
	for(int i=0;i<cdf.size(); ++i){
		pdf[i]= exp(-pow(i-cdf.size()/2.0,2)/4.0);
		cerr<<pdf[i]<<endl;
	}
	cdf[0]=pdf[0];
	for(size_t i=1;i<cdf.size(); ++i){
		cdf[i]=cdf[i-1]+pdf[i]; 
	}

	vector<int> hist(cdf.size(),0);
//    int total=0;
    uint64_t iterations = 500000000;
    auto const start = std::chrono::high_resolution_clock::now();
    for(uint64_t i=0;i<iterations; ++i){
		size_t sample=r.category_cdf(cdf.begin(), cdf.end());
		assert(sample < hist.size());
		hist[sample]++;
//		total++;
	}
    uint64_t const duration = std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now() - start).count();

	for(int i=0; i<hist.size(); ++i ){
		int barlen=hist[i]/static_cast<double>(iterations)*50;
		int j=0;
		for(j=0;j<barlen; ++j){
			cout<<"#";
		}
		for(;j<50; ++j){
			cout<<"-";
		}
		cout<<"("<<hist[i] << " -> " << (hist[i] / static_cast<double>(hist[5])) << " -> " << (hist[i] / static_cast<double>(hist[5]) - pdf[i]) <<")"<<endl;
	}
	std::cout << "Took " << (duration / 1000) << "us" << std::endl;
    std::cout << "Took " << (duration / static_cast<double>(iterations)) << "ns/sample" << std::endl;
}