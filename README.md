# Realtime Online Spatiotemporal Topic Modeling
A library for unsupervised analysis and visualization of spatiotemporal data. 

## Installation 

### MacOS deps
Requirements: [Homebrew](http://mxcl.github.io/homebrew/) package manager for installing dependencies. 

```bash
brew install boost --cxx11
```

### Linux Deps

```bash
sudo apt-get install libboost-all-dev
```

### Building 
```bash

# (required) build and install rost
cd librost
# mkdir build
# cd build
# cmake ..
# make -j4
# sudo make install
```
