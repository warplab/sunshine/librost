#include "rost/markov.hpp"
#include "rost/csv_reader.hpp"
#include "rost/word_reader.hpp"

//#include "random.hpp"
#include <map>
#include <iostream>
#include <vector>
#include <limits>
using namespace std;

#include <boost/program_options.hpp>

namespace po = boost::program_options;

po::options_description& load_rost_base_options( po::options_description & desc){
  desc.add_options()
    ("help", "help")    
    ("in.words,i",   po::value<string>()->default_value("/dev/stdin"),"timestamped word list csv file")
    ("in.words.delim",po::value<string>()->default_value(","), "delimiter used to seperate words.")

    ("out,o", po::value<string>()->default_value("/dev/stdout"),"Output histogram") 

    ("vocabsize,V",  po::value<int>()->default_value(0), "Vocabulary size. 0 => use the largest wordid as vocab size.")
    ("alpha", po::value<double>()->default_value(1.0),"histogram smoothing (only used when normalizing)")
    ("normalize","Normalize the distribution so that everything sums to 1.0")
    ;
  return desc;
}

po::variables_map read_command_line(int argc, char* argv[], po::options_description& options){

  po::variables_map args;
  po::store(po::command_line_parser(argc, argv)
      .style(po::command_line_style::default_style ^ po::command_line_style::allow_guessing)
      .options(options)
      .run(), 
      args);
  po::notify(args);

  if(args.count("help")){
    cerr<<options<<endl;
    exit(0);
  }
  return args;
}




int main(int argc, char**argv){
  po::options_description options("Given timestamped words or topics file, outputs timestamped distributions.");
  load_rost_base_options(options);  
  auto args = read_command_line(argc, argv, options);

  int V = args["vocabsize"].as<int>();
  double alpha=args["alpha"].as<double>();


  
  cerr<<"Reading data from: "<<args["in.words"].as<string>()<<endl;
  cerr<<"Writing output to: "<<args["out"].as<string>()<<endl;
  ofstream out(args["out"].as<string>());

  csv_reader in(args["in.words"].as<string>(), args["in.words.delim"].as<string>()[0]); 
  vector<int> tokens = in.get();
  int i=0;
  while(!tokens.empty()){
    vector<int> words(tokens.begin()+1, tokens.end());
    int timestamp = tokens[0];
    vector<int> hist = histogram(words, V);
    
    out<<timestamp<<",";
    if(args.count("normalize")){
      vector<float> normalized= normalize(hist,alpha); 
      out<<to_str(normalized)<<endl;
    }
    else
      out<<to_str(hist)<<endl;

    if(i++ % 100 == 0) cerr<<"time: "<<timestamp/1000.0<<"         \r";
    tokens = in.get();    
  }
  
  return 0;
}



