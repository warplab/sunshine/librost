#include<iostream>
#include<fstream>
#include<vector>
#include<cassert>
#include<sstream>
#include<unordered_map>
#include<tuple>
#include<algorithm>
#include "rost/word_reader.hpp"
using namespace std;

vector<vector<string> > read_csv(string fname, int skip_row=0, int skip_col=0){
  ifstream in(fname);
  assert(in);
  string line;
  vector<vector<string> >csv; 
  while(getline(in,line)){
    if(line[0]=='%' || line[0]=='#') continue;
    stringstream ss(line);
    vector<string> words;
    string word;
    while(std::getline(ss,word,',')){      
      //      cerr<<"W:"<<word<<" ";
      words.push_back(word);
    }
    //    cerr<<endl;
    words.erase(words.begin(), words.begin()+skip_col);
    if(!words.empty()){
      csv.push_back(words);
    }
  }
  csv.erase(csv.begin(), csv.begin()+skip_row);
  return csv;
}

tuple<vector<vector<int>>, size_t> load_labels(const vector<vector<string>>& csv,int maxl=-1){
  vector<vector<int>> out;
  size_t L=0;
  unordered_map<string,int> labelid;
  for(size_t i=1; i<csv.size(); ++i){
    int t = atoi(csv[i][0].c_str());
    //    cerr<<"time = "<<t<<"  "<<csv[i][0]<<endl;
    vector<string> labels(csv[i-1].begin()+1, csv[i-1].end());
    if(maxl>0)
      labels.resize(maxl);
    vector<int> ilabels;
    for(string& slabel: labels){
      if(labelid.find(slabel) == labelid.end()){
	labelid[slabel]=L++;
      }
      ilabels.push_back(labelid[slabel]);
    }
    while(out.size()< t ){
      out.push_back(ilabels);
    }

  }
  return make_tuple(out,L);
}

tuple<vector<vector<int>>, size_t> load_topics(const vector<vector<string>>& csv){
  vector<vector<int>> out;
  size_t K=0;
  for(size_t i=0; i<csv.size(); ++i){
    vector<int> topics;
    for(size_t j=1; j<csv[i].size(); ++j){//skip the first number which is time
      topics.push_back(atoi(csv[i][j].c_str()));
    }
    if(!topics.empty()){
      K = max((int)K, *max_element(topics.begin(), topics.end()));
      out.push_back(topics);
    }
  }
  return make_tuple(out,K);
}
 
tuple<vector<vector<int>>, int> compute_joint(vector<vector<int>>& v1, size_t S1, vector<vector<int>>& v2, size_t S2){
  vector<vector<int>> joint(S1, vector<int>(S2,1));
  int total(S1*S2);
  assert(v1.size() == v2.size());
  for(size_t t=0; t<v1.size(); ++t){
    for(auto l1: v1[t]) for(auto l2: v2[t]){ joint[l1][l2]++; total++;}
  }
  return make_tuple(joint,total);
}


tuple<vector<int>, int> compute_count(vector<vector<int>>& v, size_t S){
  vector<int> count(S,1);
  int total(S);
  for(auto a: v) for(auto b:a ){ count[b]++; total++;}
  return make_tuple(count,total);
}

void out(vector<vector<int>>& topics){
  for(auto l: topics){
    for(auto l2: l)
      cout<<l2<<" ";
    cout<<endl;
  }
}
void out(vector<int>& topics){
  for(auto l: topics){
      cout<<l<<" ";
    cout<<endl;
  }
}
int main(int argc, char* argv[]){

  string label_csv_filename=argv[1];
  string topic_csv_filename=argv[2];
  int begin_time=0;
  if(argc >= 3)
    begin_time=atoi(argv[3]);

  vector<vector<int>> topics,labels;
  size_t L,K;
  cerr<<"Reding topics CSV file: "<<topic_csv_filename<<endl;
  tie(topics,K) = load_topics(read_csv(topic_csv_filename));  
  cerr<<"Reding labels CSV file: "<<label_csv_filename<<endl;
  tie(labels,L) = load_labels(read_csv(label_csv_filename));

  cerr<<"trimming for begin time: "<<begin_time<<endl;
  topics.erase(topics.begin(), topics.begin()+begin_time);
  labels.erase(labels.begin(), labels.begin()+begin_time);
  
  cerr<<K<< " " <<L<<endl;

  vector<int> count_topics, count_labels;
  vector<vector<int>> joint;
  int topics_total, labels_total, joint_total;
  tie(joint, joint_total) = compute_joint(labels,L,topics,K);
  tie(count_labels, labels_total) = compute_count(labels,L);
  tie(count_topics, topics_total) = compute_count(topics,K);
  //  out(joint);
  //  cerr<<joint_total<<" "<<labels_total<<" "<<topics_total<<endl;
  //  out(count_labels);cout<<endl;
  //  out(count_topics);
  double mi=0;//mutual information

  for(size_t l=0;l<L; ++l){
    for(size_t k=0; k<K; ++k){
      double p_x_y = static_cast<double>(joint[l][k])/joint_total;
      double p_x = static_cast<double>(count_labels[l])/labels_total;
      double p_y = static_cast<double>(count_topics[k])/topics_total;
      mi+= p_x_y*log2(p_x_y/p_x/p_y);      
    }
  }
  
  cout<<mi<<endl;     
  return 0;
}
