#ifndef ROST_CSV_READER_HPP
#define ROST_CSV_READER_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <regex>
#include <cassert>
#include <memory>

struct csv_reader {
    std::unique_ptr<std::istream> stream;
    std::istream *streamPtr;
    uint32_t line_num = 0;
    std::string line;
    //char buffer[1024*1024];
    char const delim;

    csv_reader(std::string filename, char delim_ = ',')
            : delim(delim_) {
        if (filename == "-") {
            streamPtr = &std::cin;
            filename = "/dev/stdin";
        } else if (!filename.empty()) {
            stream = std::unique_ptr<std::ifstream>(new std::ifstream(filename));
            if (!stream->good()) throw std::invalid_argument("Bad csv file input stream.");
            streamPtr = stream.get();
        }
    }

    struct row {
        uint32_t const line_num;
        char const *const begin, *const end;
        char const* iter = begin;

        /*    row(const sregex_token_iterator& begin_, const sregex_token_iterator& end_):begin(begin_),end(end_),done(false){
          if(begin == end)
            done=true;
            }*/
        row(uint32_t line_num, char const* begin_, char const* end_) : line_num(line_num), begin(begin_), end(end_) {}

        explicit row(uint32_t line_num = 0) : line_num(line_num), begin(nullptr), end(nullptr) {}

        row(const row &r) = default;

        bool done() const {
            return iter == end;
        }

        inline void advance() {
            for (; iter != end && *iter != ','; ++iter);
            if (*iter == ',') ++iter;
        }

        inline int get_int() {
            assert(!done());
            auto const tbegin = iter;
            advance();
            char* ref;
            int const v = static_cast<int>(strtol(tbegin, &ref, 10));
            if (ref == tbegin) {
                throw std::runtime_error("Unexpected input \"" + std::string(tbegin, iter - tbegin) + "\" on line " + std::to_string(line_num) + " at column " + std::to_string(tbegin - begin) + ".");
            }
            return v;
        }

        inline float get_float() {
            assert(!done());
            auto const tbegin = iter;
            advance();
            char* ref;
            float const v = strtof(tbegin, &ref);
            if (ref == tbegin) {
                throw std::runtime_error("Unexpected input \"" + std::string(tbegin, iter - tbegin) + "\" on line " + std::to_string(line_num) + " at column " + std::to_string(tbegin - begin) + ".");
            }
            return v;
        }

        std::vector<int> get_vector_int() {
            std::vector<int> v;
            while (!done()) {
                v.push_back(get_int());
            }
            return v;
        }

        std::vector<float> get_vector_float() {
            std::vector<float> v;
            while (!done()) {
                v.push_back(get_float());
            }
            return v;
        }

    };

    std::vector<int> get() {
        row r = next_row();
        return r.get_vector_int();
    }

    std::vector<std::vector<int>> get_all_int() {
        std::vector<int> row = get();
        std::vector<std::vector<int>> rows;
        while (!row.empty()) {
            rows.push_back(row);
            row = get();
        }
        return rows;
    }


    std::vector<float> get_floats() {
        row r = next_row();
        return r.get_vector_float();
    }


    row next_row() {
        while (true) {
            line.clear();
            if (*streamPtr) {
                getline(*streamPtr, line);
                ++line_num;
                if (line.empty() || line[0] == '#') {
                    continue;
                } else {
                    return row(line_num, line.begin().base(), line.end().base());
                }
            } else {
                return row();
            }
        }
    }
};


#endif
