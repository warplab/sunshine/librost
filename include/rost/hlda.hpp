#ifndef HLDA_HPP
#define HLDA_HPP

#include <array>
#include <atomic>
#include <cassert>
#include <condition_variable>
#include <fstream>
#include <functional>
#include <iostream>
#include <iterator>
#include <list>
#include <mutex>
#include <random>
#include <set>
#include <sstream>
#include <thread>
#include <utility>

#include "rost/refinery.hpp"
#include "rost/rost_types.hpp"
#include "rost/st_topic_model.hpp"
#include "rost/word_reader.hpp"

namespace warp {
template <typename PoseT,
    typename PoseNeighborsT = neighbors<PoseT>,
    typename PoseHashT = hash_container<PoseT>,
    typename PoseEqualT = pose_equal<PoseT>>
struct hROST final : public SpatioTemporalTopicModel<PoseT, PoseNeighborsT, PoseHashT, PoseEqualT> {
    typedef SpatioTemporalTopicModel<PoseT, PoseNeighborsT, PoseHashT, PoseEqualT> Super;
    typedef PoseT pose_t;
    typedef typename PoseT::value_type pose_dim_t;
    typedef std::unordered_map<PoseT, //cell_pose
        std::tuple<
            std::vector<int>, //words
            std::vector<int>, //topics
            std::vector<pose_t> //word poses
            >,
        PoseHashT,
        PoseEqualT>
        word_data_t;
    // TODO: private:
    size_t const V, L; // vocab size, topic size
    float beta, gamma;
    std::vector<float> alpha, gamma_i;
    std::mt19937 engine;
    parallel_random random;

private:
    size_t K;
    std::uniform_int_distribution<int> uniform_K_distr;
    std::vector<std::vector<int>> nWZ; //nWZ[w][z] = freq of words of type w in topic z
    std::vector<int> weight_Z;
    std::map<int, std::vector<int>> topic_hierarchy;
    std::vector<std::mutex> global_Z_mutex;
    std::mutex global_next_k_mutex;
    int next_k;

public:
    std::vector<int> const& get_weight_Z() const
    {
        return weight_Z;
    }
    std::vector<std::vector<int>> get_nZW() const
    {
        std::vector<std::vector<int>> nZW(K, std::vector<int>(V, 0));
        for (size_t w = 0; w < V; w++) {
            for (size_t k = 0; k < K; k++) {
                nZW[k][w] = nWZ[w][k];
            }
        }
        return nZW;
    }

    std::vector<int> mask_Z; //topic mask
    bool update_global_model; //if true, with each iteration teh global topic model is updated along with the local cell topic model
        // if false, only the local cell model is updated

    hROST(size_t V_, size_t K_, size_t L_, double alpha_, double beta_, double gamma_, const PoseNeighborsT& neighbors_ = PoseNeighborsT(), const PoseHashT& pose_hash_ = PoseHashT())
        : Super(V_, neighbors_, pose_hash_)
        , V(V_)
        , K(K_)
        , L(L_)
        , beta(beta_)
        , alpha(K, alpha_)
        , gamma(gamma_)
        , gamma_i(K, 0)
        , uniform_K_distr(0, K - 1)
        , nWZ(V, std::vector<int>(K, 0))
        , weight_Z(K, 0)
        , topic_hierarchy()
        , mask_Z(K, 1)
        , global_Z_mutex(K)
        , update_global_model(true)
        , next_k(0)
    {
        addChildTopic(-1);
    }

    double perplexity(bool recompute = false)
    {
        double seq_ppx = 0;
        int n_words = 0;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        //std::lock_guard<std::mutex> lock(cells_mutex);
        for (auto& cell : this->cells) {
            if (recompute)
                estimate(*cell, true);
            n_words += cell->Z.size();
            seq_ppx += cell->perplexity;
        }
        return exp(-seq_ppx / n_words);
    }

    double perplexity(const PoseT& pose, bool recompute = false)
    {
        double seq_ppx = 0;
        int n_words = 0;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        auto cell = get_cell(this->cell_lookup[pose]);
        if (recompute)
            estimate(*cell, true);
        n_words += cell->Z.size();
        seq_ppx += cell->perplexity;

        return exp(-seq_ppx / n_words);
    }

    double time_perplexity(pose_dim_t t, bool recompute = false)
    {
        double seq_ppx = 0;
        int n_words = 0;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        //std::lock_guard<std::mutex> lock(cells_mutex);
        for (auto& pose : this->poses_for_time.lower_bound(t)->second) {
            auto cell = this->get_cell(this->cell_lookup[pose]);
            if (recompute)
                estimate(*cell, true);
            n_words += cell->Z.size();
            seq_ppx += cell->perplexity;
        }
        return exp(-seq_ppx / n_words);
    }

    std::vector<float> word_perplexity(const PoseT& pose, bool recompute = false)
    {
        auto cell = this->get_cell(this->cell_lookup[pose]);
        if (recompute)
            estimate(*cell, true);

        return cell->W_perplexity;
    }

    double topic_perplexity(const PoseT& pose)
    {
        double seq_ppx = 0;
        int n_words = 0;
        double alpha_total = accumulate(alpha.begin(), alpha.end(), 0.0);
        double W = accumulate(weight_Z.begin(), weight_Z.end(), 0) + alpha_total;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        auto cell = this->get_cell(this->cell_lookup[pose]);
        for (size_t i = 0; i < cell->nZ.size(); ++i) {
            seq_ppx -= double(cell->nZ[i]) * log(double(std::max<int>(0, weight_Z[i]) + alpha[i]) / double(W));
            assert(seq_ppx == seq_ppx);
        }
        n_words = cell->Z.size();
        if (n_words == 0)
            return 1.0;
        else
            return exp(seq_ppx / n_words);
    }

    //compute topic perplexity given a topic distribution
    // returns the geometric mean of 1/P(topics[i] | weight_Z), where weight_Z is the current global topic distribution
    double cell_perplexity_topic(const std::vector<int>& nZ)
    {
        double seq_ppx = 0;
        int n_words = 0;
        double alpha_total = accumulate(alpha.begin(), alpha.end(), 0.0);
        double W = accumulate(weight_Z.begin(), weight_Z.end(), 0) + alpha_total;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        for (size_t i = 0; i < K; ++i) {
            seq_ppx -= double(nZ[i]) * log(double(weight_Z[i] + alpha[i]) / W);
        }
        n_words += accumulate(nZ.begin(), nZ.end(), 0);
        if (n_words == 0)
            return 1.0;
        else
            return exp(seq_ppx / n_words);
    }

    // returng the geometric mean of 1/P(word[i] | nWZ, nZ), where nWZ is the current topic model (VxK), and nZ is the topic distribution prior
    // nZ could be a local or global topic distribution.
    double cell_perplexity_word(const std::vector<int>& words, const std::vector<int>& nZ) const
    {
        int weight_c = accumulate(nZ.begin(), nZ.end(), 0);
        double alpha_total = accumulate(alpha.begin(), alpha.end(), 0.0);

        double ppx_sum = 0;
        for (const int& w : words) {
            double p_word = 0;
            for (size_t k = 0; k < nZ.size(); ++k) {
                p_word += (nWZ[w][k] + beta) / (weight_Z[k] + beta * V) * (nZ[k] + alpha[k]) / (weight_c + alpha_total);
            }
            ppx_sum += log(p_word);
        }
        return exp(-ppx_sum / words.size());
    }

    std::vector<float> word_topic_perplexity(const PoseT& pose)
    {
        int W = accumulate(weight_Z.begin(), weight_Z.end(), 0) + K;
        //locks the cells std::vector so that it doesnt reallocate while accessing it.
        auto cell = this->get_cell(this->cell_lookup[pose]);
        size_t n = cell->Z.size();
        std::vector<float> result(n);
        for (size_t i = 0; i < cell->Z.size(); ++i) {
            result[i] = W / double(weight_Z[i] + 1);
        }
        return result;
    }

    void set_topic_model(activity_manager::WriteToken const& token,
            const std::vector<std::vector<int>> &new_nZW,
            const std::vector<int> &new_weight_Z) {
        if (new_nZW.empty() || new_nZW.size() < K || new_nZW[0].size() == V) { // is the <K check important?
            throw std::invalid_argument("Cannot set a nZW matrix which is smaller than original matrix");
        }
        if (new_nZW.size() != this->K) {
            // TODO: is there a better way to update K?
            this->K = new_nZW.size();
            this->alpha = std::vector<float>(K, alpha[0]);
            this->uniform_K_distr = std::uniform_int_distribution<int>(0, K-1);
            this->mask_Z.resize(K);
            this->global_Z_mutex = std::vector<std::mutex>(K);
            for (size_t w = 0; w < V; ++w) {
                nWZ[w].resize(K);
            }
        }
        if (new_weight_Z.empty()) {
            weight_Z = std::vector<int>(this->K, 0);
            for (int k = 0; k < K; ++k) {
                for (size_t w = 0; w < V; ++w) {
                    nWZ[w][k] = new_nZW[k][w];
                    weight_Z[k] += new_nZW[k][w];
                }
            }
        } else {
            for (int k = 0; k < K; ++k) {
                for (size_t w = 0; w < V; ++w) {
                    nWZ[w][k] = new_nZW[k][w];
                }
            }
            weight_Z = new_weight_Z;
        }
    }

    [[deprecated]] void set_topic_model(const std::vector<int>& new_nZW, const std::vector<int>& new_weight_Z)
    {
        assert(K * V == new_nZW.size());
        assert(new_weight_Z.size() == K);
        for (int k = 0; k < K; ++k) {
            const size_t offset = k * V;
            for (size_t w = 0; w < V; w++) {
                nWZ[w][k] = new_nZW[offset + w];
            }
        }
        weight_Z = new_weight_Z;
    }

    decltype(weight_Z) get_topic_weights() const override final
    {
        return weight_Z;
    }

    //returns the KxV topic-word distribution matrix
    decltype(nWZ) get_topic_model() const override final
    {
        return get_nZW();
    }

    //compute maximum likelihood estimate for topics in the cell for the given pose
    std::vector<int> get_ml_topics_for_pose(const PoseT& pose, bool update_ppx = false)
    {
        //std::lock_guard<std::mutex> lock(cells_mutex);
        auto cell_it = this->cell_lookup.find(pose);
        if (cell_it != this->cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            std::lock_guard<std::mutex> lock(c->cell_mutex);
            return estimate(*c, update_ppx);
        } else
            return std::vector<int>();
    }

    //compute maximum likelihood estimate for topics in the cell for the given pose
    std::tuple<std::vector<int>, double> get_ml_topics_and_ppx_for_pose(const PoseT& pose)
    {
        std::vector<int> topics;
        double ppx = 0;
        auto cell_it = this->cell_lookup.find(pose);
        if (cell_it != this->cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            std::lock_guard<std::mutex> lock(c->cell_mutex);
            topics = estimate(*c, true);
            ppx = c->perplexity;
        }
        return make_tuple(topics, ppx);
    }

    std::tuple<std::vector<int>, double> get_topics_and_ppx_for_pose(const PoseT& pose)
    {
        std::vector<int> topics;
        double ppx = 0;
        auto cell_it = this->cell_lookup.find(pose);
        if (cell_it != this->cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            topics = c->Z;
            ppx = c->perplexity;
        }
        return make_tuple(topics, ppx);
    }

    void update_cooccurence_counts(const std::vector<std::vector<int>>& delta, int m = 1)
    {
        throw std::logic_error("Not implemented");
    }
    void update_cooccurence_counts(const std::vector<std::vector<int>>& delta_sub, const std::vector<std::vector<int>>& delta_add)
    {
        throw std::logic_error("Not implemented");
    }

    void add_count(int w, int z, int c = 1)
    {
        std::lock_guard<std::mutex> lock(global_Z_mutex[z]);
        nWZ[w][z] += c;
        weight_Z[z] += c;
    }

    void relabel(int w, int z_old, int z_new)
    {
        //cerr<<"lock: "<<z_old<<"  "<<z_new<<endl;
        if (z_old == z_new)
            return;

        std::lock(global_Z_mutex[z_new], global_Z_mutex[z_old]);

        nWZ[w][z_old]--;
        weight_Z[z_old]--;
        nWZ[w][z_new]++;
        weight_Z[z_new]++;

        global_Z_mutex[z_old].unlock();
        global_Z_mutex[z_new].unlock();
        //cerr<<"U:"<<z_old<<","<<z_new<<endl;
    }

    void shuffle_topics()
    {
        for (auto& c : this->cells) {
            std::lock_guard<std::mutex> lock(c->cell_mutex);
            for (size_t i = 0; i < c->Z.size(); ++i) {
                int z_old = c->Z[i];
                int w = c->W[i];
                int z_new = uniform_K_distr(engine);
                c->nZ[z_old]--;
                c->nZ[z_new]++;
                nWZ[w][z_old]--;
                nWZ[w][z_new]++;
                weight_Z[z_old]--;
                weight_Z[z_new]++;
                c->Z[i] = z_new;
            }
        }
    }

    template <typename WordContainer>
    void add_observation(const PoseT& pose, const WordContainer& words)
    {
        add_observation(pose, words.begin(), words.end(), true, words.end(), words.end());
    }
    template <typename WordIt>
    void add_observation(const PoseT& pose, WordIt word_begin, WordIt word_end, bool update_topic_model = true)
    {
        add_observation(pose, word_begin, word_end, update_topic_model, word_end, word_end);
    }

    template <typename WordIt>
    void add_observation(const PoseT& pose, WordIt begin, WordIt end, bool update_topic_model, WordIt topicBegin, WordIt topicEnd)
    {
        Super::template add_observation<WordIt>(pose, begin, end, update_topic_model, topicBegin, topicEnd);
    }

    bool forget(int cid = -1)
    {
        if (cid < 0)
            cid = random.uniform(0, this->C - 1);
        auto c = this->get_cell(cid);
        std::lock_guard<std::mutex> lock(c->cell_mutex);
        for (size_t i = 0; i < c->W.size(); ++i) {
            add_count(c->W[i], c->Z[i], -1);
        }
        c->forget();
        c->shrink_to_fit();
        return true;
    }

    /// This is where all the magic happens.
    /// Refine the topic labels for Cell c, given the cells in its spatiotemporal neighborhood (nZg), and
    /// topic model nWZ. A gibbs sampler is used to randomly sample new topic label for each word in the
    /// cell, given the priors.

    //this funciton is for quickly computing topic labels wor the given list of words, without updating the model.
    std::vector<int> refine_ext(const std::vector<int>& words, //list of input words
        size_t n_iter = 10, //number of refine iterations
        std::vector<int> nZg = std::vector<int>() //neighborhood prior topic distribution, not including the topics for the given list of words
        ) const
    {
        return this->sampleTopics(words, n_iter, nZg);
    }

    void refine(Cell& c, bool blocking) override final
    {
        if (c.id >= this->C)
            return;

        std::unique_lock<std::mutex> guard(c.cell_mutex, std::defer_lock);
        if (blocking) {
            guard.lock();
        } else if (!guard.try_lock()) {
            return;
        }

        std::vector<int> nZg = this->neighborhood(c); //topic distribution over the neighborhood (initialize with the cell)

        std::vector<float> pz(K, 0);
        std::vector<float> cumulative_pz(K, 0);

        for (size_t i = 0; i < c.W.size(); ++i) {
            int const w = c.W[i];
            int const z = c.Z[i];
            nZg[z]--;
            auto const cz = this->computeTopicCdf(w, nZg);
            auto const z_new = random.category_cdf(cz.begin(), cz.end());
            nZg[z_new]++;

            if (update_global_model)
                relabel(w, z, z_new);
            c.relabel(i, z_new);
        }
        ++(this->refine_count);
        this->word_refine_count.fetch_add(c.W.size());
    }

    /// Estimate maximum likelihood topics for the cell
    /// This function is similar to the refine(), except instead of randomly sampling from
    /// the topic distribution for a word, we pick the most likely label.
    /// We do not save this label, but return it.
    /// the topic labels from this function are useful when we actually need to use the topic
    /// labels for some application.
    std::vector<int> estimate(Cell& c, bool update_ppx = false)
    {
        //wait_till_unpaused();
        if (c.id >= this->C)
            return std::vector<int>();

        std::vector<int> nZg = this->neighborhood(c); //topic distribution over the neighborhood (initialize with the cell)

        int weight_c = 0;
        double ppx_sum = 0;
        if (update_ppx) {
            c.W_perplexity.resize(c.W.size());
            c.perplexity = 0;
            weight_c = accumulate(c.nZ.begin(), c.nZ.end(), 0);
        }

        std::vector<double> pz(K, 0);
        std::vector<int> Zc(c.W.size());

        float alpha_total = accumulate(alpha.begin(), alpha.end(), 0.0);
        for (size_t i = 0; i < c.W.size(); ++i) {
            int w = c.W[i];
            int z = c.Z[i];
            nZg[z]--; // decrement the original topic count
            if (update_ppx)
                ppx_sum = 0;

            for (size_t k = 0; k < next_k; ++k) {
                int nkw = nWZ[w][k];
                int weight_k = weight_Z[k];
                pz[k] = (nkw + beta) / (weight_k + beta * V) * (nZg[k] + alpha[k]);

                //      if(update_ppx) ppx_sum += pz[k]/(weight_g + alpha*K);
                if (update_ppx) {
                    ppx_sum += (nkw + beta) / (weight_k + beta * V) * (c.nZ[k] + alpha[k]) / (weight_c + alpha_total);
                }
            }

            nZg[z]++; // re-increment the original topic count

            if (update_ppx) {
                c.W_perplexity[i] = ppx_sum;
                c.perplexity += log(ppx_sum);
            }
            Zc[i] = max_element(pz.begin(), pz.end()) - pz.begin();
        }
        //if(update_ppx) c.perplexity=exp(-c.perplexity/c.W.size());

        return Zc;
    }

    // topic_model interface
public:
    std::vector<int> computeMaxLikelihoodTopics(const PoseT& doc) override final
    {
        return get_ml_topics_for_pose(doc, false);
    }

    uint32_t get_num_topics() const
    {
        return K;
    }

    // topic_model interface
protected:
    int addChildTopic(int parent)
    {
        auto const new_id = next_k++;
        if (parent != -1)
            topic_hierarchy[parent].push_back(new_id);
        topic_hierarchy.insert({ new_id, std::vector<int>() });
        gamma_i[new_id] = gamma;
        return new_id;
    }

    std::vector<int> computePath(const std::vector<int>& prior)
    {
        std::lock_guard<std::mutex> k_guard(global_next_k_mutex);
        std::vector<int> path(L, 0);
        for (auto i = 1; i < L; i++) {
            auto const& parent = path.at(i - 1);
            std::lock_guard<std::mutex> guard(global_Z_mutex[parent]);
            auto& children = topic_hierarchy[parent];
            auto const new_topic_idx = children.size();
            std::vector<float> pzi(new_topic_idx + 1, 0);
            for (size_t j = 0; j < new_topic_idx; ++j) {
                auto const k = children.at(j);
                auto const weight_k = std::max<int>(0, weight_Z.at(k));
                pzi.at(j) = (weight_k + beta) / (weight_k + beta * V) * (prior.at(k) + alpha.at(k));
            }
            pzi.at(new_topic_idx) = alpha[next_k] / V * gamma;
            auto z = -1;
            do {
                auto const z_idx = random.category_pdf(pzi.begin(), pzi.end());
                z = (z_idx < new_topic_idx) ? children.at(z_idx) : this->addChildTopic(parent);
                if (z == -1 && pzi.at(new_topic_idx) == 0)
                    throw std::runtime_error("Failed to add topic to path");
                pzi.at(new_topic_idx) = 0;
            } while (z == -1);
            path.at(i) = z;
        }
        return path;
    }

    std::vector<float> computeTopicCdf(int word, const std::vector<int>& prior) override final
    {
        std::vector<int> const path = computePath(prior);
        std::vector<float> cz(K, 0);
        float sum = 0;
        for (size_t k = 0; k < K; ++k) {
            if (std::find(path.cbegin(), path.cend(), k) == path.cend())
                continue;
            int const nkw = std::max<int>(0, nWZ[word][k]);
            int const weight_k = std::max<int>(0, weight_Z[k]);
            cz[k] = sum + (nkw + beta) / (weight_k + beta * V) * (prior[k] + alpha[k]) * mask_Z[k] * gamma_i[k];
            sum = cz[k];
        }
        return cz;
    }

    void addWordObservation(int word, int newTopic, bool update_topic_model) override final
    {
        if (update_topic_model && update_global_model)
            add_count(word, newTopic);
    }

    int computeRandomTopic() const
    {
        return random.category_pdf(gamma_i.begin(), gamma_i.end());
    }

public:
    void printHierarchy(std::ostream& stream) const
    {
        auto const hierarchy = topic_hierarchy;
        std::stringstream sstream;
        sstream << "Topic Hierarchy:\n";
        for (auto const& entry : hierarchy) {
            if (entry.second.size() == 0) {
                continue;
            }
            sstream << entry.first << ": ";
            for (auto const& item : entry.second) {
                sstream << " " << item;
            }
            sstream << "\n";
        }
        std::list<std::pair<int, int>> queue;
        std::vector<std::vector<int>> level_map;
        queue.push_back({ 0, 0 });
        for (; !queue.empty();) {
            auto const& next = queue.front();
            if (next.first >= level_map.size()) {
                level_map.push_back({ next.second });
            } else {
                level_map[next.first].push_back(next.second);
            }
            for (auto const& child : hierarchy.at(next.second)) {
                queue.push_back({ next.first + 1, child });
            }
            queue.pop_front();
        }
        for (auto i = 0; i < level_map.size(); i++) {
            sstream << "Level " << i << ":";
            for (auto const& item : level_map[i]) {
                sstream << " " << item;
            }
            sstream << "\n";
        }
        stream << sstream.str() << std::endl;
    }
};
}

#endif // HLDA_HPP
