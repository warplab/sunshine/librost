#ifndef ROST_TYPES_HPP
#define ROST_TYPES_HPP

#include <vector>
#include <cstddef>
#include <mutex>
#include <cmath>

//maps the v to [-N,N)
template<typename T>
T standardize_angle( T v, T N=180){
  T r = v%(2*N);
  r = r >= N ? r-2*N: r;
  r = r < -N ? r+2*N: r;
  return r;
}

template<typename Pose>
int pose_distance(const Pose& p1, const Pose& p2){
  int d=0;
  for(size_t i=0;i<p1.size(); ++i){
    d+= std::abs(p1[i] - p2[i]);
  }
  return d;
}
template<typename Pose>
struct pose_equal{
  bool operator()(const Pose& l, const Pose& r)const{
    for(size_t i=0;i<l.size(); ++i){
      if(l[i] != r[i]) return false;
    }
    return true;
  }
};

template<typename Pose>
struct pose_equal_ignoretime{
  bool operator()(const Pose& l, const Pose& r)const{
    for(size_t i=1;i<l.size(); ++i){
      if(l[i] != r[i]) return false;
    }
    return true;
  }
};

template<>
int pose_distance<int>(const int& p1, const int& p2){
  return std::abs(p1-p2);
}
//neighbors specialization for a pose of array type
template<typename Array>
struct neighbors_hypercube {
  //  const int depth;
  Array depth;
  size_t neighborhood_size;
  explicit neighbors_hypercube( int depth_){
    depth.fill(depth_);
    //neighborhood_size=depth.size()*2*depth[0];
    neighborhood_size=1;
    for(auto d: depth){
      neighborhood_size *= (d*2+1);
    }
    //neighborhood_size-=1;
  }
  explicit neighbors_hypercube( const Array& depth_):depth(depth_){
    neighborhood_size=1;
    depth=depth_;
    for(auto d: depth){
      neighborhood_size *= (d*2+1);
    }
    //neighborhood_size-=1;
  }
  std::vector<Array> operator()(const Array& o) const{

    std::vector<Array> neighbor_list(neighborhood_size);

    for(size_t i=0; i<neighbor_list.size(); ++i){
      int j=i;
      Array& pose = neighbor_list[i];
      for(size_t d=0;d<depth.size(); ++d){
        auto width=(depth[d]*2+1);
        pose[d]=(j%width - depth[d]);
        j=j/width;
      }
    }
    return neighbor_list;
  }
};


//neighbors specialization for a pose of array type
template<typename Array>
struct neighbors {
  //  const int depth;
  Array depth;
  size_t neighborhood_size;
  explicit neighbors( int depth_){
    depth.fill(depth_);
    neighborhood_size=depth.size()*2*depth[0];
  }
  explicit neighbors( const Array& depth_):depth(depth_){
    neighborhood_size=0;
    for(auto d:  depth){
      neighborhood_size += 2*d;
    }
  }
  std::vector<Array> operator()(const Array& o) const{

    std::vector<Array> neighbor_list(neighborhood_size, o);

    auto outit = neighbor_list.begin();
    for(size_t i=0; i<o.size(); ++i){
      for(int d = 0; d<depth[i]; ++d){
        (*outit++)[i] += d+1;
        (*outit++)[i] -= d+1;
      }
    }
    return neighbor_list;
  }
};

template<>
struct neighbors<int> {
  int depth;
  neighbors(int depth_):depth(depth_){}
  std::vector<int> operator()(int o) const{
    std::vector<int> neighbor_list(2*depth,o);
    auto i = neighbor_list.begin();
    for(int d=1;d<=depth; ++d){
      (*i++)+=d;
      (*i++)-=d;
    }
    return neighbor_list;
  }
};

//Spatial std::hash function
template<typename T>
struct hash_container{
  std::hash<typename T::value_type> hash1;
  size_t operator()(const T& pose) const {
    size_t h(0);
    for(size_t i=0;i<pose.size(); ++i){
      if(sizeof(size_t)==4){
        h = (h<<7) | (h >> 25);
      }
      else{
        h = (h<<11) | (h >> 53);
      }
      h = hash1(pose[i]) ^ h;
    }
    return h;
  }
};

//this std::hash functor ignores the first dim of the pose (time)
//while computing the std::hash
template<typename T>
struct hash_pose_ignoretime{
  std::hash<typename T::value_type> hash1;
  size_t operator()(const T& pose) const {
    size_t h(0);
    for(size_t i=1;i<pose.size(); ++i){
      if(sizeof(size_t)==4){
        h = (h<<7) | (h >> 25);
      }
      else{
        h = (h<<11) | (h >> 53);
      }
      h = hash1(pose[i]) ^ h;
    }
    return h;
  }
};

//outer product of dist with itself.
std::vector<std::vector<int>> cooccurence_counts(std::vector<int>& dist){
  std::vector<std::vector<int>> cc(dist.size(),std::vector<int>(dist.size(),0));
  for(size_t i=0;i<dist.size();++i){
    for(size_t j=0;j<dist.size(); ++j){
      cc[i][j]=dist[i]*dist[j];
    }
  }
  return cc;
}

/// All observed data is stored in Cell structs. Each cell contains
/// all observed word and their topic labels that have the same saptiotemporal
/// location. A Cell is connected to its spatiotemporal neighbors
struct Cell{
  size_t id;
  std::vector<size_t> neighbors;
  std::vector<int> W; //word labels
  std::vector<int> Z; //topic labels
  //std::vector<int> nW; //distribution/count of W
  std::vector<int> nZ; //distribution/count of Z
  std::vector<std::vector<int>> nZZ; //couccrance counts of Z (KxK matrix)

  std::vector<float> W_perplexity; // perplexity of each word
  std::mutex cell_mutex;
  double perplexity = 0;
  Cell(size_t id_, size_t vocabulary_size, size_t topics_size)
      : Cell(id_, topics_size)
  {
  }
  Cell(size_t id_, size_t topics_size):
    id(id_),
    nZ(topics_size, 0)
  {
    neighbors.reserve(24); //TOFIX: access to neighbors list should be protected using std::mutex.
  }
  std::vector<int> get_topics(){
    std::lock_guard<std::mutex> lock(cell_mutex);
    return Z;
  }
  std::pair<int, int> get_wz(int i){
    cell_mutex.lock();
    auto r=std::make_pair(W[i],Z[i]);
    cell_mutex.unlock();
    return r;
  }
  void relabel(size_t i, int z_new){
    int const z_old = Z[i];
    Z[i]=z_new;
    nZ[z_old]--;
    nZ[z_new]++;
  }
  void shrink_to_fit(){
    neighbors.shrink_to_fit();
    W.shrink_to_fit();
    Z.shrink_to_fit();
    //nW.shrink_to_fit();
    nZ.shrink_to_fit();
    //Z_mutex.shrink_to_fit();
  }
  void forget(){
    W.clear();
    Z.clear();
    for(auto &z:nZ)z=0;
  }
  void update_cooccurence_counts(){
    nZZ = cooccurence_counts(nZ);
  }
};

template<typename T>
struct scaled_plus{
  T scale;
  explicit scaled_plus(const T& scale_):scale(scale_){}
  T operator()(const T& v1, const T& v2) const{
    return v1 + v2/scale;
  }
};

#endif // ROST_TYPES_HPP
