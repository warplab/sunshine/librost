#pragma once

#include "rost/markov.hpp"
#include "rost/rost_types.hpp"
#include "rost/topic_model.hpp"
#include <algorithm>
#include <map>
#include <set>
#include <unordered_map>

namespace warp {

template <typename PoseT,
    typename PoseNeighborsT = neighbors<PoseT>,
    typename PoseHashT = hash_container<PoseT>,
    typename PoseEqualT = pose_equal<PoseT>>
struct SpatioTemporalTopicModel : public topic_model<Cell, PoseT> {
    typedef PoseT pose_t;
    typedef typename PoseT::value_type pose_dim_t;
    typedef PoseNeighborsT neighbors_t;
    typedef std::unordered_map<PoseT, //cell_pose
        std::tuple<
            std::vector<int>, //words
            std::vector<int>, //topics
            std::vector<pose_t> //word poses
            >,
        PoseHashT,
        PoseEqualT>
        word_data_t;

protected:
    PoseNeighborsT neighbors; //function to compute neighbors
    PoseHashT pose_hash;
    std::atomic<size_t> refine_count, word_refine_count; //number of cells/words refined till now;
    mutable std::mutex cells_mutex; //lock for cells, since cells can grow in size

    std::map<pose_dim_t, std::set<PoseT>> poses_for_time; // stores list of poses for each time step in sorted order

public: // TODO: Remove
    std::vector<std::shared_ptr<Cell>> cells;
    size_t C; // #cells
    std::vector<PoseT> cell_pose;
    std::unordered_map<PoseT, size_t, PoseHashT, PoseEqualT> cell_lookup;
    float g_sigma; //neighborhood decay

public:
    SpatioTemporalTopicModel(size_t const vocab_size,
        const PoseNeighborsT& neighbors_ = PoseNeighborsT(),
        const PoseHashT& pose_hash_ = PoseHashT())
        : topic_model<Cell, PoseT, int, int>(vocab_size)
        , neighbors(neighbors_)
        , pose_hash(pose_hash_)
        , refine_count(0)
        , word_refine_count(0)
        , cell_lookup(10000000, pose_hash)
        , C(0)
        , g_sigma(0.5)
    {
    }

    // TODO: Add perplexity metrics

    std::shared_ptr<Cell> get_cell(const PoseT& pose) const
    {
        std::lock_guard<std::mutex> lock(cells_mutex);
        auto it = cell_lookup.find(pose);
        assert(it != cell_lookup.end());
        return cells[it->second];
    }

    std::shared_ptr<Cell> get_cell(size_t cid) const
    {
        std::lock_guard<std::mutex> lock(cells_mutex);
        return cells[cid];
    }

    //topics in the cell for the given pose
    std::vector<int> get_topics_for_pose(const PoseT& pose)
    {
        //std::lock_guard<std::mutex> lock(cells_mutex);
        auto cell_it = cell_lookup.find(pose);
        if (cell_it != cell_lookup.end()) {
            auto c = this->get_cell(cell_it->second);
            return c->Z;
        } else
            return std::vector<int>();
    }

    decltype(poses_for_time) const& get_poses_by_time() const {
        return poses_for_time;
    }

    size_t num_cells()
    {
        return C;
    }

    void addObservations(PoseT const& doc, std::vector<int> const& observations,
        std::vector<int> const& topics = {},
        bool update_model = true) override
    {
        add_observation(doc, observations.begin(), observations.end(), update_model,
            (topics.size() > 0) ? topics.begin() : topics.end(), topics.end());
    }

    template <typename WordIt, typename TopicIt = int*>
    void add_observation(const PoseT& pose, WordIt word_begin, WordIt word_end, bool update_topic_model, TopicIt topic_begin = nullptr, TopicIt topic_end = nullptr)
    {
        auto cell_it = cell_lookup.find(pose);
        bool newcell = false;
        std::shared_ptr<Cell> c;
        if (cell_it == cell_lookup.end()) {
            c = std::make_shared<Cell>(C, this->get_num_topics());
            cells_mutex.lock();
            cells.push_back(c);
            cell_pose.push_back(pose);
            cells_mutex.unlock();

            c->cell_mutex.lock();
            //add neighbors to the cell
            for (auto& g : neighbors(pose)) {
                auto g_it = cell_lookup.find(g);
                if (g_it != cell_lookup.end()) {
                    auto gc = get_cell(g_it->second);
                    //  cerr<<gc->id<<" ";
                    gc->neighbors.push_back(c->id);
                    c->neighbors.push_back(gc->id);
                }
            }
            //      cerr<<endl;
            cell_lookup[pose] = c->id;
            newcell = true;
        } else {
            c = get_cell(cell_it->second);
            c->cell_mutex.lock();
        }

        bool havetopics = (topic_begin != topic_end);

        //do the insertion
        for (auto wi = word_begin; wi != word_end; ++wi) {
            int w = *wi;
            c->W.push_back(w);
            //generate random topic label

            int z;
            if (havetopics) {
                z = *topic_begin;
                topic_begin++;
            } else {
                z = this->computeRandomTopic();
            }
            assert(z < this->get_num_topics());
            c->Z.push_back(z);
            //update the histograms
            c->nZ[z]++;
            this->addWordObservation(w, z, update_topic_model);
        }

        c->shrink_to_fit();

        if (newcell) {
            C++;
        }
        poses_for_time[pose[0]].insert(pose);

        c->cell_mutex.unlock();
    }

    //returns the topic distribution in the neighborhood of a cell
    std::vector<int> neighborhood(Cell& c) const
    {
        std::vector<int> nZg(this->get_num_topics(), 0); //topic distribution over the neighborhood (initialize to 0s)

        //accumulate topic histogram from the neighbors
        for (auto gid : c.neighbors) {
            if (gid < C && gid != c.id) {
                auto g = get_cell(gid);
                auto dist = pose_distance(cell_pose[gid], cell_pose[c.id]);
                //        int scale = (1<<dist);
                float scale = pow(g_sigma, dist);
                //        int scale=1;
                for (size_t k = 0; k < nZg.size(); ++k) {
                    nZg[k] += ceil(g->nZ[k] * scale);
                }
                //transform(g->nZ.begin(), g->nZ.end(), nZg.begin(), nZg.begin(), [&](int x, int y){return x/scale +y;} );
            }
        }
        transform(c.nZ.begin(), c.nZ.end(), nZg.begin(), nZg.begin(), std::plus<int>()); // add this cell's topic dist (nZ) to the returned one (nZg)
        return nZg;
    }

    virtual double cell_perplexity_word(const std::vector<int>& words, const std::vector<int>& nZ) const = 0;
    virtual std::tuple<std::vector<int>,double> get_ml_topics_and_ppx_for_pose(const PoseT& pose) = 0;

    // topic_model interface
public:
    std::shared_ptr<Cell> getDocument(const PoseT& doc) const override
    {
        return get_cell(doc);
    }

    size_t get_refine_count() const
    {
        return refine_count.load();
    }

    size_t get_word_refine_count() const
    {
        return word_refine_count.load();
    }
};
}
