#ifndef ROST_IO_HPP
#define ROST_IO_HPP

#include <iostream>
#include <vector>
#include <numeric>
#include "rost/csv_reader.hpp"

template<int N>
std::string to_string(std::array<int,N>& pose, char delim=','){
  std::string s="";
  if(pose.empty()) return s;
  s += std::to_string(pose[0]);
  for(size_t i=1;i<N;++i){
    s+=delim+std::to_string(pose[i]);
  }
  return s;
}

template<typename T>
std::string to_string(std::vector<T>& v, char delim=','){
  std::string s="";
  if(v.empty()) return s;
  s+=std::to_string(v[0]);
  for(size_t i=1;i<v.size();++i){
    s+=delim+std::to_string(v[i]);
  }
  return s;
}

template<typename It>
std::string to_string_iter(It begin, It end){
  std::vector<typename It::value_type> v(begin,end);
  return to_string(v);
}

void out(std::ostream& out_topics, int timestamp, std::vector<int>& topics){
  //  if(topics.empty()) return;
  out_topics<<timestamp;
  for(size_t i=0;i<topics.size(); ++i){
    out_topics<<","<<topics[i];
  }
  out_topics<<std::endl;
}

template<typename Pose>
void out(std::ostream& out_topics, const std::vector<Pose>& poses, const std::vector<int>& topics){
  //  if(topics.empty()) return;
  int timestamp=poses[0][0];

  out_topics<<timestamp;

  for(size_t i=0;i<topics.size(); ++i){
    int next_timestamp=poses[i][0];
    if(next_timestamp!=timestamp){
      timestamp = next_timestamp;
        out_topics<<std::endl<<timestamp;
    }
    out_topics<<","<<topics[i];
  }
  out_topics<<std::endl;
}


template<typename Pose>
void out_poses(std::ostream& out, const std::vector<Pose>& poses){
  //  if(topics.empty()) return;
  int timestamp=poses[0][0];

  out<<timestamp;

  for(size_t i=0;i<poses.size(); ++i){
    int next_timestamp=poses[i][0];
    if(next_timestamp!=timestamp){
      timestamp = next_timestamp;
        out<<std::endl<<timestamp;
    }
    for(size_t d=1;d<poses[i].size();d++){
      out<<","<<poses[i][d];
    }
  }
  out<<std::endl;
}

struct Logger{
  std::ofstream olog;
  explicit Logger(std::string filename):olog(filename.c_str()){
  }
};
template<typename T>
Logger& operator<<(Logger& log, const T& s){
  log.olog<<s;
  std::cerr<<s;
  log.olog.flush();
  return log;
}



//load words and collect them by their poses
template<typename R>
std::tuple<
  std::map<typename R::pose_t, std::vector<typename R::pose_t>>, //[cellpose]->{wordposes..}
  std::vector<int> //timestamps
>  
    load_words(R& rost, const std::string& wordfile, const std::string& topicfile, const std::string& posefile, int cell_time, int cell_space, bool update_topic_model, bool retime){
  typedef typename R::pose_t pose_t;
  constexpr size_t posedim = pose_t().size();

  csv_reader in_words(wordfile);
  csv_reader in_topics(topicfile);
  csv_reader in_poses(posefile);

  std::vector<int> word_line, pose_line, topic_line;

  std::map<pose_t, std::vector<int>> words_for_pose;
  std::map<pose_t, std::vector<int>> topics_for_pose;
  std::map<pose_t, std::vector<pose_t> > exactposes_for_pose; //saves the pose of each word before cellularization
  std::vector<int> timestamps;

  bool have_topics= (topicfile != "");
  bool have_poses= (posefile != "");

  word_line = in_words.get();
  if(have_poses) {pose_line = in_poses.get(); assert (posedim > 1); std::cerr<<"Will try to read poses\n";}
  if(have_topics) {topic_line = in_topics.get(); std::cerr<<"Will try to read topics\n";}

  int ti=0;
  while(!word_line.empty()){
    //read a line of words
    std::vector<int> words(word_line.begin()+1, word_line.end());
    timestamps.push_back(word_line[0]);
    //build the correponding poses
    std::vector<pose_t> cell_poses(words.size());
    std::vector<pose_t> exact_poses(words.size());

    //first pose dimension is always time
    for(auto& p: cell_poses){
      if(retime)
	p[0]=ti;
      else
	p[0]=word_line[0]/cell_time; //cellularized pose
    }
    for(auto& p: exact_poses) p[0]=word_line[0];

    //if have space dimension, read them
    if(posedim>1){
      assert(!pose_line.empty());
      //pose line is in format timestamp,x1,y1,x2,y2,x3,y3.....
      if(pose_line.size() != (words.size()*(posedim-1) +1) ){
        std::cerr<<"ERROR: poses are not synced."<<std::endl;
        assert(false);
      }
      for(size_t i=0;i<cell_poses.size(); ++i){
        for(size_t d=1;d<posedim; ++d){
          exact_poses[i][d]=pose_line[i*(posedim-1)+d];
          cell_poses[i][d]=(exact_poses[i][d])/cell_space;
        }
      }     
    }

   //collect words by their similar pose
    for(size_t i=0;i<words.size(); ++i){
      words_for_pose[cell_poses[i]].push_back(words[i]);
      exactposes_for_pose[cell_poses[i]].push_back(exact_poses[i]);
    }


    if(have_topics){
      if(topic_line[0]!=word_line[0] && topic_line.size() != word_line.size()){
        std::cerr<<"ERROR: topics and words time stamps are not synced"<<std::endl;
        assert(false);
      }
      std::vector<int> topics(topic_line.begin()+1, topic_line.end());
         //collect words by their similar pose
      for(size_t i=0;i<words.size(); ++i){
        topics_for_pose[cell_poses[i]].push_back(topics[i]);
      }   
    }

    std::cerr<<"Read "<<ti<<" timesteps                 \r";
    word_line = in_words.get();
    if(have_poses) pose_line = in_poses.get();
    if(have_topics) topic_line = in_topics.get();
    ti++;
  }    
  std::cerr<<std::endl;
  //add the words and topics to rost
  for(auto it = words_for_pose.begin(); it!= words_for_pose.end(); it++){
    pose_t pose=it->first;
    if(have_topics)
      rost.add_observation( pose,
                            words_for_pose[pose].begin(), words_for_pose[pose].end(),
                            update_topic_model,
                            topics_for_pose[pose].begin(), topics_for_pose[pose].end()                        
                          );
    else
      rost.add_observation( pose,
                            words_for_pose[pose].begin(), words_for_pose[pose].end(),
                            update_topic_model,
                            words_for_pose[pose].end(),words_for_pose[pose].end()// no topics
                          );

  }
  return make_tuple(exactposes_for_pose, timestamps);
}


/// Loads a topic mask file into rost
//template<typename R>
//bool load_topic_mask(R&rost, const std::string& filename){
//  if(!filename.empty()) {
//    std::vector<int> topicmask;
//    std::ifstream maskin(filename);
//    int m;
//    maskin >>m;
//    std::cerr<<"Reading mask: ";
//    while(maskin){
//      std::cerr<<m<<" ";
//      if(m!=0 && m!=1){
//        std::cerr<<"ERROR: reading mask file. mask values must be 0 or 1.\n";
//        return false;
//      }
//      topicmask.push_back(m);
//      maskin >> m;
//    }
//    assert(rost.mask_Z.size()==topicmask.size());
//    rost.mask_Z = topicmask;
//    return true;
//  }
//  else
//    return false;
//}

template<typename R>
bool load_topic_model(R& rost, const std::string& filename){
    //read the topic model as a CSV file
  std::cerr<<"Loading topic model: "<<filename<<"\n";
  csv_reader topic_model_reader(filename,','); 
  std::vector<int> topic_model;
  std::vector<int> topic_weights;
  std::vector<int> topic = topic_model_reader.get();
  while(!topic.empty()){
    topic_model.insert(topic_model.end(), topic.begin(), topic.end());
    topic_weights.push_back(std::accumulate(topic.begin(), topic.end(), 0));
    topic = topic_model_reader.get();
  }
  rost.set_topic_model(topic_model, topic_weights);
  return true;
}

template<typename R>
bool save_topic_model(R& rost, const std::string& filename){
  //std::cerr<<"Writing topicmodel to: "<<filename<<std::endl;
  std::ofstream out_topic_model(filename);
  std::vector<std::vector<int> > topic_model = rost.get_topic_model();
  for(auto & topic: topic_model){
    out_topic_model<<topic[0];
    for(size_t i = 1; i< topic.size(); ++i){
      out_topic_model<<","<<topic[i];
    }
    out_topic_model<<std::endl;
  }
  return true;
}


template<typename R>
void write_poses(R& rost, const std::string& filename, const std::map<typename R::pose_t,std::vector<typename R::pose_t>>& wordposes){

  typedef typename R::pose_t pose_t;

  std::ofstream out(filename);
  //  wordposes element is a std::tuple(pose_t, vetor<pose_t>)
  std::map<int, std::vector<pose_t> > poses_by_time;

  //order poses by their original timestamp
  for(auto it= wordposes.begin(); it != wordposes.end(); ++it){
    pose_t cell_pose = it->first;
    auto& word_poses = it->second;

    for(size_t i=0;i<word_poses.size(); ++i){
      int timestamp=word_poses[i][0];
      poses_by_time[timestamp].push_back(word_poses[i]);
//      insert(poses_by_time[timestamp].end(), word_poses.begin(), word_poses.end());
    }
  }

  for(auto it=poses_by_time.begin(); it!=poses_by_time.end(); ++it){
    out<<it->first;
    std::vector<pose_t>& poses= it->second;
    for(size_t i=0;i<poses.size(); ++i){
      for(size_t d=1;d<poses[i].size(); ++d){
        out<<","<<poses[i][d];
      }
    }
    out<<std::endl;
  }
}
template<typename R>
void write_poses(R& rost, const std::string& filename, const   typename R::word_data_t& wordposes){

  typedef typename R::pose_t pose_t;

  std::ofstream out(filename);
  //  wordposes element is a std::tuple(pose_t, vetor<pose_t>)
  std::map<int, std::vector<pose_t> > poses_by_time;

  //order poses by their original timestamp
  for(auto it= wordposes.begin(); it != wordposes.end(); ++it){
    pose_t cell_pose = it->first;
    auto& word_poses = std::get<2>(it->second);

    for(size_t i=0;i<word_poses.size(); ++i){
      int timestamp=word_poses[i][0];
      poses_by_time[timestamp].push_back(word_poses[i]);
//      insert(poses_by_time[timestamp].end(), word_poses.begin(), word_poses.end());
    }
  }

  for(auto it=poses_by_time.begin(); it!=poses_by_time.end(); ++it){
    out<<it->first;
    std::vector<pose_t>& poses= it->second;
    for(size_t i=0;i<poses.size(); ++i){
      for(size_t d=1;d<poses[i].size(); ++d){
        out<<","<<poses[i][d];
      }
    }
    out<<std::endl;
  }
}

template<typename R>
void write_topics(R& rost, const std::string& filename, const std::map<typename R::pose_t,std::vector<typename R::pose_t>>& wordposes, bool maxlikelihood){
  typedef typename R::pose_t pose_t;

  std::ofstream out_topics(filename);
  //  wordposes element is a std::tuple(pose_t, vetor<pose_t>)
  std::map<int, std::vector<int> > topics_by_time;

  for(auto it= wordposes.begin(); it != wordposes.end(); ++it){
    pose_t cell_pose = it->first;
    auto& word_poses = it->second;

    std::vector<int> topics;
    if(maxlikelihood)
      topics = rost.get_ml_topics_for_pose(cell_pose);
    else
      topics = rost.get_topics_for_pose(cell_pose);

    for(size_t i=0;i<topics.size(); ++i){
      int timestamp=word_poses[i][0];
      topics_by_time[timestamp].push_back(topics[i]);
//      topics_by_time[timestamp].insert(topics_by_time[timestamp].end(), topics.begin(), topics.end());
    }
  }

  for(auto it=topics_by_time.begin(); it!=topics_by_time.end(); ++it){
    out_topics<<it->first;
    std::vector<int>& topics= it->second;
    for(size_t i=0;i<topics.size(); ++i){
      out_topics<<","<<topics[i];
    }
    out_topics<<std::endl;
  }
}

template<typename R>
void write_topics(R& rost, const std::string& filename, const typename R::word_data_t& wordposes, bool maxlikelihood){
  typedef typename R::pose_t pose_t;

  std::ofstream out_topics(filename);
  //  wordposes element is a std::tuple(pose_t, vetor<pose_t>)
  std::map<int, std::vector<int> > topics_by_time;

  for(auto it= wordposes.begin(); it != wordposes.end(); ++it){
    pose_t cell_pose = it->first;
    auto& word_poses = std::get<2>(it->second);

    std::vector<int> topics;
    if(maxlikelihood)
      topics = rost.get_ml_topics_for_pose(cell_pose);
    else
      topics = rost.get_topics_for_pose(cell_pose);

    for(size_t i=0;i<topics.size(); ++i){
      int timestamp=word_poses[i][0];
      topics_by_time[timestamp].push_back(topics[i]);
//      topics_by_time[timestamp].insert(topics_by_time[timestamp].end(), topics.begin(), topics.end());
    }
  }

  for(auto it=topics_by_time.begin(); it!=topics_by_time.end(); ++it){
    out_topics<<it->first;
    std::vector<int>& topics= it->second;
    for(size_t i=0;i<topics.size(); ++i){
      out_topics<<","<<topics[i];
    }
    out_topics<<std::endl;
  }
}

//writes perplexity for each cell
template<typename R, typename PoseMap>
void write_cell_ppx(R& rost, const std::string& filename, const PoseMap& wordposes){
  typedef typename R::pose_t pose_t;

  std::ofstream out(filename);

  for(auto it= wordposes.begin(); it != wordposes.end(); ++it){
    pose_t cell_pose = it->first;
    out<<cell_pose[0];
    for(size_t i=1; i<cell_pose.size(); ++i){
      out<<","<<cell_pose[i];
    }
    out<<","<<rost.perplexity(cell_pose,false)<<std::endl;
  }
}

//writes perplexity for each cell
template<typename R>
void write_word_perplexity(R& rost, const std::string& filename, const typename R::word_data_t& wordposes, int ppxtype=0){
  typedef typename R::pose_t pose_t;

  std::ofstream out(filename);
  //  wordposes element is a std::tuple(pose_t, vetor<pose_t>)
  std::map<int, std::vector<float> > by_time;

  for(auto it= wordposes.begin(); it != wordposes.end(); ++it){
    pose_t cell_pose = it->first;
    auto& word_poses = std::get<2>(it->second);

    std::vector<float> w_ppx;
    switch(ppxtype){
      case 0: w_ppx = rost.word_perplexity(cell_pose); break;
      case 1: w_ppx = rost.word_topic_perplexity(cell_pose); break;
    } 

    for(size_t i=0;i<w_ppx.size(); ++i){
      int timestamp=word_poses[i][0];
      by_time[timestamp].push_back(w_ppx[i]);
//      topics_by_time[timestamp].insert(topics_by_time[timestamp].end(), topics.begin(), topics.end());
    }
  }

  for(auto it=by_time.begin(); it!=by_time.end(); ++it){
    out<<it->first;
    auto & data= it->second;
    for(size_t i=0;i<data.size(); ++i){
      out<<","<<data[i];
    }
    out<<std::endl;
  }
}

template<typename R>
void write_time_perplexity(R& rost, const std::string& filename, std::vector<int>&timestamps, int cell_time, bool recompute){
  std::ofstream out(filename);

  for(int t: timestamps){
    out<<t<<","<<rost.time_perplexity(t/cell_time,recompute)<<std::endl;
  }
}

template<typename R>
void write_time_perplexity(R& rost, const std::string& filename, std::vector<int>&timestamps, bool recompute){
  std::ofstream out(filename);

  int i=0;
  for(int t: timestamps){
    out<<t<<","<<rost.time_perplexity(i++,recompute)<<std::endl;
  }
}

template<typename P>
void  write_path(const std::string& filename, const std::vector<P>& path, const P&cell_size){
  std::ofstream out(filename);
  for(auto& p: path){
    out<<p[0]*cell_size[0] + cell_size[0]/2;
    for(size_t d=1;d<cell_size.size(); ++d){
      out<<","<<p[d]*cell_size[d] + cell_size[d]/2;
    }
    out<<std::endl;
  }
}


#endif
